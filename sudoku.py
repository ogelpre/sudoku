#!/usr/bin/env python3

from argparse import ArgumentParser
from itertools import product

CHARS = " 123456789"

class Arguments:
    def __init__(self):
        parser = ArgumentParser(
            description='solve sudoku')

        parser.add_argument('file', help='sudoku file')

        self.parser = parser

    @property
    def args(self):
        args = self.parser.parse_args()

        return args


class ParserError(Exception):
    pass


class Parser:
    def __init__(self):
        self._raw_data = None
        self._sudoku = None

    def load_file(self, file):
        with open(file) as fstream:
            self._raw_data = fstream.readlines()
        self._sudoku = None

    def parse(self):
        grid = {}
        for lno, line_raw in enumerate(self._raw_data):
            if lno > 8:
                raise ParserError('Too many lines.')
            line = line_raw.rstrip('\n')
            if len(line) > 9:
                raise ParserError("Line {} to long".format(lno +1))
            for colno, char in enumerate(line):
                if char not in CHARS:
                    raise ParserError("Char '{}' not allowed".format(char))
                if char.isdigit():
                    char_ = int(char)
                else:
                    char_ = None
                grid[(lno, colno)] = char
        self._sudoku = SudokuGrid(grid)

    @property
    def sudoku(self):
        return self._sudoku

class Box:
    def __init__(self, value = None):
        self._value = value
        if value is None:
            self._candidates = set(range(1,10))
        else:
            self._candidates = set()

    @property
    def value(self):
        return self._value

    @property
    def candidates(self):
        return self.candidates

    def discard_candidate(self, value):
        self._candidates.discard(value)
        if len(self._candidates) == 1:
            self._value = self._candidates.pop()

class SudokuGrid:
    def __init__(self, data = None):
        if data is None:
            self._grid = dict.fromkeys(product(range(0,9), range(0,9)), Box())
        else:
            self._grid = dict(zip(data.keys(), map(Box, data.values())))

    @property
    def printable(self):
        raise NotImplementedError

class Main:
    def __init__(self, sudoku_file):
        self._sudoku_file = sudoku_file

    def run(self):
        parser = Parser()
        parser.load_file(self._sudoku_file)
        parser.parse()
        sudoku = parser.sudoku

if __name__ == '__main__':
    args = Arguments().args
    main = Main(args.file)
    main.run()
